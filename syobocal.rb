# -*- coding: utf-8-*-
require 'net/http'
require 'date'
require 'active_support/core_ext'

# しょぼいカレンダーのモジュール群
class Syobocal

  def initialize(setDate = Date.today,set4CoolTIDs = "",setIgnoreTIDs = "",setChFilter = "")
    @fourCoolTIDs = set4CoolTIDs.split(",")
    @ignoreTIDs = setIgnoreTIDs.split(",")
    @date = setDate
    @chFilter = setChFilter
  end

  # 4クールアニメを設定する
  def set4CoolTIDs(new4CoolTIDs)
    @fourCoolTIDs = new4CoolTIDs
  end

  # 無視するアニメを設定する
  def setIgnoreTIDs(newIgnoreTIDs)
    @ignoreTIDs = newIgnoreTIDs
  end

  # 日付を設定する
  def setDate(newDate)
    @date = newDate
  end

  # チャンネルフィルタを設定する
  def setChFilter(newChFilter)
    @chFilter = newChFilter
  end

  # カレンダー情報を取得。cal_chk.phpのレスポンスを返す。JSONのおかげで使わなくてよくなった。
  def get_cal_chk(cookie = nil)
    url = URI.parse('http://cal.syoboi.jp/cal_chk.php?days=20&cookie=1')
    req = Net::HTTP::Get.new(url)
    req['cookie'] = cookie
    res = Net::HTTP.start(url.host,url.port){|http|
      http.request(req)
    }
    return res.body
  end
  private :get_cal_chk

  # アニメ情報を取得。json.phpのProgramByDate,Title*のレスポンスを返す(JSON)
  def getAnimeInfo_JSON(days = 20,req = "TitleMedium")
    start = @date.strftime("%Y-%m-%d")
    url = URI.parse("http://cal.syoboi.jp/json.php?Req=ProgramByDate,#{req}&start=#{start}&days=#{days}")
    req = Net::HTTP::Get.new(url)
    res = Net::HTTP.start(url.host,url.port){|http|
      http.request(req)
    }
    return res.body
  end

  # 今期放送の新規アニメを判定する
  def isNewAnimeOr2CoolAnime?(animeInfo)

    ret = false

    tid = animeInfo["TID"]
    animeInfoDate = Date.new(animeInfo["FirstYear"].to_i == 0 ? 1 : animeInfo["FirstYear"].to_i,
                             animeInfo["FirstMonth"].to_i == 0 ? 1 : animeInfo["FirstMonth"].to_i,
                             @date.day)
    firstEndYear = animeInfo["FirstEndYear"].to_i
    cat = animeInfo["Cat"].to_i

    now = @date
    next3Month = @date.months_since(2)
    before3Month = @date.months_ago(3)

    # 年が本日と同じ年 かつ カテゴリがアニメ
    if (animeInfoDate.year == @date.year) && (cat == 1) then
      # 今期放送の新規アニメを洗い出す
      if (animeInfoDate >= now) && (animeInfoDate <= next3Month) then
        ret = true
      # 2クールを洗い出す
      elsif (animeInfoDate >= before3Month) && (animeInfoDate < now) && (firstEndYear == 0) then
        ret = true
      end
    end
    # 3,4クールは洗い出せないので直接記述する
    if @fourCoolTIDs.include?(tid) then
      ret = true
    end
    # 無視するアニメは無視する
    if @ignoreTIDs.include?(tid) then
      ret = false
    end

    return ret
  end
  private :isNewAnimeOr2CoolAnime?

  # タイトル一覧を取得。タイトル一覧をTextで返す
  def getAnimeTitle(syobojson)
    animeTitles = ""

    animeInfos = JSON.parse(syobojson)

    animeInfos["Titles"].keys.each do |key|
      if isNewAnimeOr2CoolAnime?(animeInfos["Titles"]["#{key}"]) then
        animeTitles += animeInfos["Titles"]["#{key}"]["Title"] + " : " + getAnimeFirstTimeAndFirstCh(animeInfos,key) + "\n"
      end
    end
    return animeTitles
  end

  def getAnimeFirstTimeAndFirstCh(programs,tid)
    firstTime = 9999999999
    firstCh = ""

    programs["Programs"].keys.each do |key|
      if programs["Programs"]["#{key}"]["TID"] == tid then
        chFiltering = false
        if @chFilter.empty? then
          chFiltering = true
        elsif !@chFilter.split(",").include?(programs["Programs"]["#{key}"]["ChID"]) then
          chFiltering = true
        end

        if chFiltering == true then
          if programs["Programs"]["#{key}"]["StTime"].to_i < firstTime then
            firstTime = programs["Programs"]["#{key}"]["StTime"].to_i
            firstCh = programs["Programs"]["#{key}"]["ChName"]
          end
        end
      end
    end
    return firstCh + "(" + Time.at(firstTime).strftime("%Y-%m-%d %H:%M:%S") + ")"
  end

  # しょぼいカレンダーのdb.phpのTitleLookUpで取得したCommentから
  # 必要な情報のみ抜き出す関数
  def getFormatComment(comment)
    # アウトラインのタイトルフラグ一覧
    none = 0
    link = 1
    staff = 2
    opORed = 3

    # アウトラインのタイトルフラグ(初期値は0)
    flag = 0

    formatComment = ""
    comment.lines() { |line|
      if line.include?("*") then
        if line.include?("リンク") then
          formatComment += "【リンク】\n"
          flag = link
        elsif line.include?("スタッフ") then
          formatComment += "【スタッフ】\n"
          flag = staff
        elsif line.include?("オープニングテーマ") then
          formatComment += "【オープニングテーマ】\n"
          # タイトルのみをマッチさせ、代入
          if /オープニングテーマ\d?「([^」]*)」/ =~ line then
            formatComment += "曲名:" + $1 + "\n"
            flag = opORed
          end
        elsif line.include?("エンディングテーマ") then
          formatComment += "【エンディングテーマ】\n"
          if /エンディングテーマ\d?「([^」]*)」/ =~ line then
            formatComment += "曲名:" + $1 + "\n"
            flag = opORed
          end
        else
          flag = none
        end
      end

      if flag == link then
        if /-\[\[([^h]*)(http[^\]]*)\]\]/ =~ line then
          formatComment += $1.chop! + ":" + $2 + "\n"
        end
      elsif flag == staff then
        if line.include?("原作") ||
            line.include?("監督") ||
            line.include?("ディレクター") ||
            line.include?("シリーズ構成") ||
            line.include?("脚本") ||
            line.include?("キャラクターデザイン") ||
            line.include?("音楽") ||
            line.include?("制作") ||
            line.include?("製作") then
          unless line.include?("作画") ||
              line.include?("音響") ||
              line.include?("美術") ||
              line.include?("撮影") ||
              line.include?("ゲスト") ||
              line.include?("統括") ||
              line.include?("補佐") ||
              line.include?("CG") ||
              line.include?("協力") then
            # 先頭の:を取り除く
            formatComment += line.sub(":","")
          end
        end
      elsif flag == opORed then
        if line.include?("作詞") ||
            line.include?("作曲") ||
            line.include?("編曲") ||
            line.include?("歌") then
          # 先頭の:を取り除く
          formatComment += line.sub(":","")
        end
      else
      end
    }
    return formatComment
  end
  private :getFormatComment

  # アニメの情報を取得。情報をテキストで返す。
  def getAnimeInfo(syobojson)
    animeInfo = ""

    animeInfos = JSON.parse(syobojson)

    animeInfos["Titles"].keys.each do |key|
      if isNewAnimeOr2CoolAnime?(animeInfos["Titles"]["#{key}"]) then
        animeInfo += "【タイトル】\n"
        animeInfo += animeInfos["Titles"]["#{key}"]["Title"] + "\n"
        animeInfo += getFormatComment(animeInfos["Titles"]["#{key}"]["Comment"]) + "\n"
      end
    end
    return animeInfo
  end
end

# 実験用
test = Syobocal.new(Date.new(2014,9,25),"2722,3275,3346,3434","","3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,49,50,51,52,53,55,56,57,59,60,61,62,63,65,68,69,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219")

json = File.read("test.json")

puts test.getAnimeTitle(json)
