# -*- coding: utf-8-*-
require 'net/http'
require 'rexml/document'
require 'date'

# しょぼいカレンダーのカレンダー情報をXMLで取得
# チャンネルフィルターを設定
# cookie = "ChFilter=3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,49,50,51,52,53,55,56,57,59,60,61,62,63,65,68,69,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219"

# url = URI.parse('http://cal.syoboi.jp/cal_chk.php?cookie=1&days=30')
url = URI.parse('http://cal.syoboi.jp/cal_chk.php?days=20')
req = Net::HTTP::Get.new(url)
# req['cookie'] = cookie
res = Net::HTTP.start(url.host,url.port){|http|
  http.request(req)
}

# XMLを取得し、tidArrayにTIDを格納する
tidArray = Array.new
doc = REXML::Document.new(res.body)
doc.elements.each('syobocal/ProgItems/ProgItem') do |element|
  tidArray.push(element.attributes["TID"])
end

# 重複しているTIDを削除
tidArray.uniq!
tidParam = ''
tidArray.each {|tid|
    tidParam += tid + ","
}
# 末尾の,を削除する
tidParam.chop!

# しょぼいカレンダーのdb.phpからスタッフ情報を取得
# 先頭から順にTID,タイトル、タイトルよみ、スタッフ情報、初回放送年、初回放送月、カテゴリが取得できる
path = "http://cal.syoboi.jp/db.php?Command=TitleLookup&" + "TID=" + tidParam + "&Fields=TID,Title,TitleYomi,Comment,FirstYear,FirstMonth,Cat,FirstEndYear"
url = URI.parse(path)
req = Net::HTTP::Get.new(url)
res = Net::HTTP.start(url.host,url.port){|http|
  http.request(req)
}

# 取得したXMLからデータをハッシュ配列に格納
doc = REXML::Document.new(res.body)
staffItems = Array.new
date = Date.today
# date = Date.new(2014,9,25)
doc.elements.each('TitleLookupResponse/TitleItems/TitleItem') do |tItem|
  staff = Hash.new

  staff["TID"] = tItem.children[0].text.to_i
  staff["Title"] =tItem.children[1].text
  staff["TitleYomi"] = tItem.children[2].text
  staff["Comment"] = tItem.children[3].text
  staff["FirstYear"] = tItem.children[4].text.to_i
  staff["FirstMonth"] = tItem.children[5].text.to_i
  staff["Cat"] = tItem.children[6].text.to_i
  staff["FirstEndYear"] = tItem.children[7].text.to_i

  nowMonth = date.month
  next3Month = (date + 60).month
  before3Month = (date - 110).month
  # 年が本日と同じ年 かつ カテゴリがアニメ
  if (staff["FirstYear"] == date.year) && (staff["Cat"] == 1) then
    # 今期放送の新規アニメを洗い出す
    if ((staff["FirstMonth"] >= nowMonth) && (staff["FirstMonth"] <= next3Month)) then
      staffItems.push(staff)
    # 2クールを洗い出す
    elsif (staff["FirstMonth"] >= before3Month && staff["FirstMonth"] < nowMonth) && staff["FirstEndYear"] == 0 then
      staffItems.push(staff)
    end
  end
  # 4クールは洗い出せないので直接記述する…
  if (staff["TID"] == 2722 || staff["TID"] == 3275 || staff["TID"] == 3346 || staff["TID"] == 3434) then
    staffItems.push(staff)
  end
end

# 重複しているデータを削除
staffItems.uniq!

# 年とシーズンを求める
year = date.year

if (date.month <= 2) then
  season = "冬"
if (date.month <= 5) then
  season = "春"
elsif (date.month <= 8) then
  season = "夏"
elsif (date.month <= 11) then
  season = "秋"
else
  season = "冬"
  year += 1
end

# 出力
puts year.to_s + season + "アニメメモ"
puts "視聴決定"
staffItems.each {|staff|
  puts staff["Title"]
}
puts
puts "気が向いたら"
puts
puts "完走したもの"
puts
puts "映画"
