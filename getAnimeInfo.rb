# -*- coding: utf-8-*-
require 'net/http'
require 'rexml/document'
require 'date'

# しょぼいカレンダーのdb.phpのTitleLookUpで取得したCommentから
# 必要な情報のみ抜き出す関数
def getFormatComment(comment)
  # アウトラインのタイトルフラグ一覧
  none = 0
  link = 1
  staff = 2
  opORed = 3

  # アウトラインのタイトルフラグ(初期値は0)
  flag = 0

  formatComment = ""
  comment.lines() { |line|
    if line.include?("*") then
      if line.include?("リンク") then
        formatComment += "【リンク】\n"
        flag = link
      elsif line.include?("スタッフ") then
        formatComment += "【スタッフ】\n"
        flag = staff
      elsif line.include?("オープニングテーマ") then
        formatComment += "【オープニングテーマ】\n"
        # タイトルのみをマッチさせ、代入
        if /オープニングテーマ\d?「([^」]*)」/ =~ line then
          formatComment += "曲名:" + $1 + "\n"
          flag = opORed
        end
      elsif line.include?("エンディングテーマ") then
        formatComment += "【エンディングテーマ】\n"
        if /エンディングテーマ\d?「([^」]*)」/ =~ line then
          formatComment += "曲名:" + $1 + "\n"
          flag = opORed
        end
      else
        flag = none
      end
    end

    # 実験用
#    flag = opORed

    if flag == link then
      if /-\[\[([^h]*)(http[^\]]*)\]\]/ =~ line then
        formatComment += $1.chop! + ":" + $2 + "\n"
      end
    elsif flag == staff then
      if line.include?("原作") ||
          line.include?("監督") ||
          line.include?("ディレクター") ||
          line.include?("シリーズ構成") ||
          line.include?("脚本") ||
          line.include?("キャラクターデザイン") ||
          line.include?("音楽") ||
          line.include?("制作") ||
          line.include?("製作") then
        unless line.include?("作画") ||
            line.include?("音響") ||
            line.include?("美術") ||
            line.include?("撮影") ||
            line.include?("ゲスト") ||
            line.include?("統括") ||
            line.include?("補佐") ||
            line.include?("CG監督") ||
            line.include?("協力") then
          # 先頭の:を取り除く
          formatComment += line.sub(":","")
        end
      end
    elsif flag == opORed then
      if line.include?("作詞") ||
          line.include?("作曲") ||
          line.include?("編曲") ||
          line.include?("歌") then
        # 先頭の:を取り除く
        formatComment += line.sub(":","")
      end
    else
    end
    }
  return formatComment
end


res = File.read("animeInfo.xml")

date = Date.today

year = date.year
month = date.month

if month <= 2 then
  month = 12
  year -= 1
elsif month <= 5 then
  month = 3
elsif month <= 8 then
  month = 6
elsif month <= 11 then
  month = 9
else
  month = 12
end

date = Date.new(year , month , 25)

doc = REXML::Document.new(res)
staffItems = Array.new
doc.elements.each('TitleLookupResponse/TitleItems/TitleItem') do |tItem|
  staff = Hash.new

  staff["TID"] = tItem.children[0].text.to_i
  staff["Title"] =tItem.children[1].text
  staff["TitleYomi"] = tItem.children[2].text
  staff["Comment"] = tItem.children[3].text
  staff["FirstYear"] = tItem.children[4].text.to_i
  staff["FirstMonth"] = tItem.children[5].text.to_i
  staff["Cat"] = tItem.children[6].text.to_i
  staff["FirstEndYear"] = tItem.children[7].text.to_i

  nowMonth = date.month
  next3Month = (date + 60).month
  before3Month = (date - 110).month
  # 年が本日と同じ年 かつ カテゴリがアニメ
  if (staff["FirstYear"] == date.year) && (staff["Cat"] == 1) then
    # 今期放送の新規アニメを洗い出す
    if ((staff["FirstMonth"] >= nowMonth) && (staff["FirstMonth"] <= next3Month)) then
      staffItems.push(staff)
    # 2クールを洗い出す
    elsif (staff["FirstMonth"] >= before3Month && staff["FirstMonth"] < nowMonth) && staff["FirstEndYear"] == 0 then
      staffItems.push(staff)
    end
  end
  # 4クールは洗い出せないので直接記述する…
  if (staff["TID"] == 2722 || staff["TID"] == 3275 || staff["TID"] == 3346 || staff["TID"] == 3434) then
    staffItems.push(staff)
  end
end

# 重複しているデータを削除
staffItems.uniq!

# 年とシーズンを求める
year = Date.today.year

if (date.month <= 2) then
  season = "冬"
elsif (date.month <= 5) then
  season = "春"
elsif (date.month <= 8) then
  season = "夏"
elsif (date.month <= 11) then
  season = "秋"
else
  season = "冬"
  year += 1
end

# 出力
puts year.to_s + season + "アニメメモ"


staffItems.each {|staff|
  info = getFormatComment(staff["Comment"])
  puts "【タイトル】",staff["Title"],info
  puts
}
